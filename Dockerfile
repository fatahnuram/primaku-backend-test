FROM node:16.16-alpine3.16

WORKDIR /app
COPY package.json package-lock.json .
RUN npm install

COPY . .

CMD npm run start
